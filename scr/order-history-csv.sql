SELECT 
    orderid,
    uid,
    IF(type='BTC', 'SELL', 'BUY'),
    IF(type='BTC', initial_amount, initial_want_amount) amount,
    IF(type='BTC', initial_want_amount/initial_amount, initial_amount/initial_want_amount) amount,
    timest,
    status
FROM orderbook
INTO OUTFILE '/tmp/orders.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'

