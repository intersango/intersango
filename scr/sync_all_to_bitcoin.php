<?php
require '../errors.php';
require '../util.php';

$query = "SELECT MAX(uid) AS max_uid FROM users";
$result = do_query($query);

if(!has_results($result)) {
    echo "Failed to get max uid\n";
    exit(0);
}

$result = get_row($result);

$max_uid = $result['max_uid'];

for ($uid=0;$uid<$max_uid;$uid++) {
    # check they actually exist
    $query = "SELECT 1 FROM users WHERE uid='$uid'";
    $result = do_query($query);
    if (has_results($result)) {
        sync_to_bitcoin($uid);
        echo "Done.\n";
    } else {
        echo "User $uid doesn't exist.\n";
    }
}
?>
