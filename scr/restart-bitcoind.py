#!/usr/bin/env python2
import subprocess
import jsonrpc
import socket
import time

jsonrpc.authproxy.HTTP_TIMEOUT = 30

rpc = jsonrpc.ServiceProxy("http://:@localhost:8332/")
bitcoind = subprocess.Popen(['./bitcoind','-daemon','-keypool=1000','-paytxfee=0','-nolisten'])
time.sleep(30)

while True:
    try:
        print(rpc.getbalance())
    except socket.timeout:
        print("socket.timeout")
        subprocess.Popen(['killall','-9','bitcoind']).wait()
        bitcoind = subprocess.Popen(['./bitcoind','-daemon'])
        time.sleep(30)
    except socket.error:
        print("socket.timeout")
        subprocess.Popen(['killall','-9','bitcoind']).wait()
        bitcoind = subprocess.Popen(['./bitcoind','-daemon'])
        time.sleep(30)
    finally:
        time.sleep(30)
