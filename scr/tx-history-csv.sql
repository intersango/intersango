SELECT 
    txid,
    uid,
    IF(type='BTC', a_amount, b_amount) amount,
    IF(type='BTC', b_amount/a_amount, a_amount/b_amount) price
FROM transactions
JOIN orderbook
ON transactions.a_orderid=orderbook.orderid
WHERE b_amount>0
INTO OUTFILE '/tmp/trades1.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
;

SELECT 
    txid,
    uid,
    IF(type='BTC', b_amount, a_amount) amount,
    IF(type='BTC', a_amount/b_amount, b_amount/a_amount) price
FROM transactions
JOIN orderbook
ON transactions.b_orderid=orderbook.orderid
WHERE b_amount>0
INTO OUTFILE '/tmp/trades2.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
;

