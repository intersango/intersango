import MySQLdb
import decimal

def numstr_to_internal(amount):
    return decimal.Decimal(amount) * 10**8

def internal_to_numstr(internal):
    return decimal.Decimal(internal) / 10**8

def parse_amount_field(amount):
    req_type = amount[0]
    amount = amount[1:]
    internal_amount = numstr_to_internal(amount)
    assert(internal_to_numstr(internal_amount) == decimal.Decimal(amount))
    assert(req_type == '+' or req_type == '-')
    if req_type == '-':
        internal_amount = -internal_amount
    return int(internal_amount)

def find_user_uid(cursor, deposref):
    cursor.execute("""
        SELECT uid
        FROM users
        WHERE deposref='%s'
        """%deposref)
    rows = cursor.fetchall()
    if not rows:
        return None
    return rows[0][0]

def mark_entry(cursor, bid, status):
    cursor.execute("""
        UPDATE bank_statement
        SET status='%s'
        WHERE bid='%i'
        """%(status, bid))

def create_request(cursor, uid, amount, bid):
    cursor.execute("""
        INSERT INTO requests (
            req_type,
            curr_type,
            uid,
            amount
        ) VALUES (
            'DEPOS',
            'GBP',
            '%i',
            '%i'
        )
        """%(uid, amount))
    cursor.execute("""
        UPDATE bank_statement
        SET reqid=LAST_INSERT_ID()
        WHERE
            bid='%i'
            AND status='PROCES'
            AND reqid IS NULL
        """%bid)

def find_request_id(cursor,amount,name):
    cursor.execute("""
    SELECT requests.reqid
    FROM requests,uk_requests
    WHERE requests.reqid=uk_requests.reqid
    AND amount=%s
    AND name=%s
    AND status='FINAL'""",(amount,name))
    
    requests = cursor.fetchall()
    
    if len(requests) > 1 or len(requests) == 0:
        return False
    else:
        return requests[0][0]
        

def parse_deposits_hsbc():
    db_handle = MySQLdb.connect('localhost', 'root', '', 'intersango')
    cursor = db_handle.cursor()
    cursor.execute("""
        SELECT 
            bid, 
            entry
        FROM bank_statement 
        WHERE 
            bank_name='HSBC'
            AND status='VERIFY'
        ORDER BY bid ASC
        """)
    entries = cursor.fetchall()
    for bid, entry in entries:
        mark_entry(cursor, bid, 'PROCES')
        timest, name_info, amount = entry.split(',')
        amount = parse_amount_field(amount)

        name = name_info.split(' ')
        deposref = name.pop()
        name = " ".join(name)

        uid = find_user_uid(cursor, deposref)
        if uid is None and amount > 0:
            print 'BAD deposref:', deposref
            mark_entry(cursor, bid, 'BADREF')
            continue
        
        if amount > 0:
            create_request(cursor, uid, amount, bid)
            mark_entry(cursor, bid, 'FINAL')
        else:
            mark_entry(cursor, bid, 'PAYOUT')
            reqid = find_request_id(cursor,-amount,name)
            if reqid is not False:
                cursor.execute("""
                UPDATE bank_statement
                SET reqid=%s
                WHERE bid=%s""",(reqid,bid))
            
        print 'Parsed:', deposref, 'for user', uid

if __name__ == '__main__':
    parse_deposits_hsbc()

