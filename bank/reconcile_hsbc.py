import MySQLdb
import decimal

def reconcile_hsbc():
    db_handle = MySQLdb.connect('localhost', 'root', '', 'intersango')
    cursor = db_handle.cursor()
    cursor.execute("""
        SELECT
            bid,
            entry,
            reqid
        FROM bank_statement
        WHERE
            bank_name='HSBC'
            AND status='PAYOUT'
            AND reqid IS NOT NULL
        GROUP BY reqid
        HAVING COUNT(reqid) > 1
        """)
    entries = cursor.fetchall()
    for bid, entry, reqid in entries:
        print("Bank statement: ",bid,entry,reqid)
        cursor.execute("""
        SELECT req_type, uid, amount, curr_type, name, bank, acc_num, sort_code
        FROM requests, uk_requests
        WHERE requests.reqid=uk_requests.reqid
        AND requests.reqid=%s""",(reqid,))

        result = cursor.fetchone()

        if result is not None:
            req_type, uid,amount,curr_type,name,bank,acc_num,sort_code = result
            
            assert(req_type=="WITHDR")
            
            print("Request",uid,amount,curr_type,bank,acc_num,sort_code)
            
            cursor.execute("""
            SELECT bid
            FROM bank_statement
            WHERE reqid=%s""",(reqid,))
            
            bids = cursor.fetchall()
            
            for other_bid in bids[1:]:
                print("bid",other_bid)
                
                other_bid = other_bid[0]
                cursor.execute("""
                INSERT INTO requests(req_type,curr_type,status,uid,amount)
                VALUES ('WITHDR','GBP','FINAL',%s,%s)""",(uid,amount))
                
                new_reqid = cursor.lastrowid
                
                cursor.execute("""
                INSERT INTO uk_requests(reqid,name,bank,acc_num,sort_code)
                VALUES (%s,%s,%s,%s,%s)""",(new_reqid,name,bank,acc_num,sort_code))
                
                cursor.execute("""
                UPDATE bank_statement
                SET reqid=%s
                WHERE bid=%s """,(new_reqid,other_bid))
                
                cursor.execute("""
                UPDATE purses
                SET amount=amount-%s
                WHERE
                    type='GBP'
                    AND uid=%s""",(amount,uid))
                
                print("new request id",new_reqid)

if __name__ == '__main__':
    reconcile_hsbc()

