import withdraw_helper

def prompt(message):
    while True:
        cont = raw_input(message + ' ')
        if cont == '' or cont == 'y' or cont == 'Y':
            return True
        elif cont == 'n' or cont == 'N':
            return False

def mark_withdrawal(cursor, reqid, new_status, prev_status):
    cursor.execute("""
        UPDATE requests
        SET status=%s
        WHERE
            reqid=%s
            AND status=%s
        """, (new_status, reqid, prev_status))

cursor = withdraw_helper.cursor()
withdrawals = withdraw_helper.get_withdrawals(cursor, 'PROCES')

count = 1
for reqid, amount, name, bank, acc_num, sort_code in withdrawals:
    print count, '/', len(withdrawals)
    print
    count += 1

    # truncate decimals to 2 places
    amount = withdraw_helper.quantize(amount)

    print 'Name =\t\t', name
    print 'Account =\t', acc_num

    if len(sort_code) != 6:
        print 'Sort code is wrong length'
        print 'Request ID =\t', reqid
        mark_withdrawal(cursor, reqid, 'BAD', 'PROCES')
        continue

    print 'Sort code =\t', sort_code
    print
    print 'Amount =\t', amount

    if len(acc_num) != 8:
        print 'Account number is not 8 digits'
        print acc_num
        print 'Request ID =\t', reqid
        mark_withdrawal(cursor, reqid, 'BAD', 'PROCES')
        continue

    form_info = """javascript:function foo(){
    document.getElementById("benName").value="%s";
    document.getElementById("benSortCode").value="%s";
    document.getElementById("benAccountNumber").value="%s";
    document.getElementById("benReference").value="%s";
    document.getElementById("paymentAmount").value="%s";
    document.getElementById("paymentASAPIndicator").checked=true;
    PC_7_2_VI_submitform(document.PC_7_2_VI_thirdpar);
    } foo();"""%(name, sort_code, acc_num, "Britcoin", amount)

    withdraw_helper.copy_to_clipboard(form_info)

    print
    print 'Add person.'

    # can set badsort/acc
    if not prompt('Continue with this person? [y]/n'):
        if prompt('Mark as bad? [y]/n'):
            mark_withdrawal(cursor, reqid, 'BAD', 'PROCES')
        else:
            mark_withdrawal(cursor, reqid, 'NONE', 'PROCES')
        continue

    mark_withdrawal(cursor, reqid, 'AWAIT', 'PROCES')
    print "-----------------------------------"

