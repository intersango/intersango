<?php
require '../util.php';

$query = "
    SELECT 1
    FROM requests
    WHERE
        req_type='WITHDR'
        AND status='PROCES'
        AND curr_type='GBP'
    LIMIT 1
    ";
$result = do_query($query);
if (has_results($result))
{
    echo "Last set of withdrawals was left in an incomplete state.\n";
    echo "Please resolve this before continuing.\n";
    exit(-1);
}

$query = "
    UPDATE requests
    SET status='PROCES'
    WHERE
        req_type='WITHDR'
        AND status='VERIFY'
        AND curr_type='GBP'
    ";
do_query($query);

