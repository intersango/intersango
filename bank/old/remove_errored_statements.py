import MySQLdb

db = MySQLdb.connect("localhost", "root", "", "intersango")
c = db.cursor()
c.execute("""
    SELECT
        bid,
        bank_statement.reqid,
        amount,
        requests.uid,
        entry
    FROM bank_statement
    JOIN requests
    ON bank_statement.reqid=requests.reqid
    WHERE
        bank_name='HSBC'
        AND bank_statement.status='ERROR'
    """)
for bid, reqid, amount, uid, entry in c.fetchall():
    c.execute("""
        SELECT amount
        FROM purses
        WHERE
            uid='%i'
            AND type='GBP'
        """%uid)
    ns = c.fetchall()
    assert(len(ns) == 1)
    balance = ns[0]
    if (balance < amount):
        print 'Not enough cash balance=%i amount=%i'%(balance, amount)
        print uid
    c.execute("""
        UPDATE purses
        SET amount=amount-'%i'
        WHERE
            uid='%i'
            AND type='GBP'
        """%(amount, uid))
    c.execute("""
        UPDATE requests
        SET status='REVERS'
        WHERE reqid='%i'
        """%reqid)
    c.execute("""
        UPDATE bank_statement
        SET status='REVERS'
        WHERE
            bank_name='HSBC'
            AND status='ERROR'
            AND bid='%i'
        """%bid)

