import MySQLdb
import decimal

db = MySQLdb.connect("localhost", "root", "", "intersango")

cursor = db.cursor()

cursor.execute("LOCK TABLES orderbook WRITE, purses WRITE, transactions WRITE, requests WRITE, requests AS requests_select WRITE, uk_requests WRITE, uk_requests AS uk_requests_select WRITE")

cursor.execute("""
SELECT uid, amount
FROM purses
WHERE
    amount < 0
    AND type='GBP'
""")

overwithdrawn_accounts = cursor.fetchall()

for uid, purse_amount in overwithdrawn_accounts:
    purse_amount = abs(purse_amount)
    
    print uid, purse_amount
    
    cursor.execute("""
    SELECT reqid, amount
    FROM requests
    WHERE
        req_type='WITHDR'
        AND curr_type='GBP'
        AND status='VERIFY'
        AND uid=%s
    """, (uid,))

    pending_withdrawals = cursor.fetchall()

    last_reqid = None
    for reqid, req_amount in pending_withdrawals:
        print '\t', reqid, req_amount
        cursor.execute("""
        UPDATE requests
        SET status='REJECT'
        WHERE reqid=%s
        """, (reqid,))

        cursor.execute("""
        UPDATE purses
        SET amount=amount+%s
        WHERE
            type='GBP'
            AND uid=%s
        """, (req_amount, uid))

        last_reqid = reqid

    cursor.execute("""
        SELECT amount
        FROM purses
        WHERE
            type='GBP'
            AND uid=%s
        """, (uid, ))
        
    balance = cursor.fetchone()[0]

    if balance > 0 and last_reqid is not None:
        cursor.execute("""
        INSERT INTO requests(req_type, uid, curr_type, status, amount)
        SELECT req_type, uid, curr_type, 'VERIFY', %s
        FROM requests AS requests_select WHERE reqid=%s
        """, (balance, last_reqid))
        
        cursor.execute("""
        INSERT INTO uk_requests(reqid, name, bank, acc_num, sort_code)
        SELECT LAST_INSERT_ID(), name, bank, acc_num, sort_code
        FROM uk_requests AS uk_requests_select WHERE reqid=%s
        """, (last_reqid,))

cursor.execute("UNLOCK TABLES")
